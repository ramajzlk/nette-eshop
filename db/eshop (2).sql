-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1:3308
-- Vytvořeno: Pát 15. dub 2022, 13:46
-- Verze serveru: 10.4.24-MariaDB
-- Verze PHP: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `eshop`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `orders`
--

CREATE TABLE `orders` (
  `idorders` int(11) NOT NULL,
  `idusers` int(11) NOT NULL,
  `date` date NOT NULL,
  `town` varchar(45) NOT NULL,
  `street` varchar(45) NOT NULL,
  `email` varchar(70) NOT NULL,
  `idproducts` binary(1) NOT NULL,
  `stav` varchar(30) NOT NULL,
  `psc` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `products`
--

CREATE TABLE `products` (
  `idproducts` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `description` varchar(200) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(70) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `products`
--

INSERT INTO `products` (`idproducts`, `name`, `description`, `price`, `category`, `image`) VALUES
(1, 'DMXGEAR elasťáky, modré.', 'Elasťáky DMXGEAR jsou jedny z nejpohodlnějších kalhot, které jsou na této planetě. K dodání pouze v modré barvě.', 1000, 'Elastaky', 'https://dmxgear.cz/38634-thickbox_default/dmxgear-panske-kompresni-elastaky-pro-athlete-multicolor-sedo-modre.jpg'),
(2, 'Asics Hyper MD 6', 'Firma Asics je špičkou na trhu s obuví. Boty řady Hyper MD 6 Vám nabízí prvotřídní zážitek z běhu.', 1500, 'Boty', 'https://eshop.total-sport.cz/katalog-obrazku/produkt-1256/nahled.jpg?z=1464350668'),
(3, 'Triko na běhání.', 'Chcete obyčejné triko na běhání, kterému se každý zasměje? Proto je tu pro Vás toto triko.', 200, 'Trika', 'https://www.topshirt.cz/nahled-u/motivace-behani.jpg');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `idusers` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(90) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `role` varchar(30) NOT NULL,
  `email` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`idusers`, `username`, `password`, `name`, `surname`, `role`, `email`) VALUES
(1, 'admin', '$2y$10$MbUWux6xm.OyZAAjcDu9EOlxtfql4dtJBBWRG7iiajk9xW5K8jLx.', 'admin', 'admin', 'admin', 'bla@mail.com');

--
-- Indexy pro exportované tabulky
--

--
-- Indexy pro tabulku `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`idorders`);

--
-- Indexy pro tabulku `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`idproducts`);

--
-- Indexy pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idusers`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `orders`
--
ALTER TABLE `orders`
  MODIFY `idorders` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `products`
--
ALTER TABLE `products`
  MODIFY `idproducts` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `idusers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
